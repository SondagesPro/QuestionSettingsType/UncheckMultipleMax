/**
 * @file function for unchecking when max is set
 * @author Denis Chenu
 * @copyright 2016 Advantages <https://advantages.fr/>
 * @copyright 2016 Denis Chenu <http://www.sondages.pro>
 * @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat (MIT)
 */

function uncheckMultipleMax(qid) {
    $(document).on('change', "#question" + qid + " :checkbox", function () {
        var maxChecked = $("#max-answers-" + qid).text().trim() * 1;
        if (maxChecked && $(this).prop("checked")) {
            if ($("#question" + qid + " :checkbox:checked").length > maxChecked) {
                if (typeof window.templateCore.alertSurveyDialog === "function") {
                    window.templateCore.alertSurveyDialog([$("#vmsg_" + qid + "_num_answers").text()]);
                } else {
                    alert($("#vmsg_" + qid + "_num_answers").text());
                }
                $(this).prop("checked", false).triggerHandler("click");
            }
        }
    });
}
