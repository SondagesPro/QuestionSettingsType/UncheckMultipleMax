<?php

/**
 * A limesurvey plugin to show an alert when user click on all buttons on mulmtiple checkbox
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class UncheckMultipleMax extends PluginBase
{
    protected static $name = 'UncheckMultipleMax';
    protected static $description = 'Disabmle click more than maximum checkbox.';

    protected $storage = 'DbStorage';
    protected $settings = [
            'alwaysActivated' => [
                'name' => 'alwaysActivated',
                'type' => 'select',
                'label' => 'Activated',
                'options' => [
                    '0' => 'Activated if there are maximum set',
                    '-1' => 'Disable'
                ],
                'default' => 0,
            ],
            'hideMaxTip' => [
                'name' => 'hideMaxTip',
                'type' => 'select',
                'label' => 'Hide tip',
                'options' => [
                    '1' => 'Forced : always hide max tip',
                    '0' => 'Automatic (if there are only a maximum)',
                    '-1' => 'No, do not hide max tip'
                ],
                'default' => 0,
            ],
    ];

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        $this->subscribe('beforeQuestionRender', 'setUncheckMultipleMax');
        $this->subscribe('newQuestionAttributes', 'addUncheckMultipleMaxAttribute');
    }

    /**
    * Add the script if needed
    */
    public function setUncheckMultipleMax()
    {
        $oEvent = $this->getEvent();
        if ($oEvent->get('type') != "M") {
            return;
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($oEvent->get('qid'));
        if (trim($aAttributes['max_answers']) == '' || $aAttributes['UncheckMultipleMax'] == -1) {
            return;
        }
        if ($aAttributes['UncheckMultipleMax'] == 0 && $this->get('alwaysActivated', null, null, 0) == -1) {
            return;
        }
        $maxDynamic = LimeExpressionManager::ProcessString($aAttributes['max_answers'], $oEvent->get('qid'));
        $newElement = Chtml::tag(
            "div",
            [
                'id' => 'max-answers-' . $oEvent->get('qid'),
                'class' => 'hidden hide',
                'style' => 'display:none'
            ],
            $maxDynamic
        );
        $oEvent->set('answers', $oEvent->get('answers') . "\n" . $newElement);
        $this->registerUncheckMultipleMaxPackage();
        $script = "uncheckMultipleMax(" . $oEvent->get('qid') . ");\n";
        if (
            $aAttributes['UncheckMultipleMaxHideTip'] == 1
            || ($aAttributes['UncheckMultipleMaxHideTip'] == 0 && $this->get('hideMaxTip', null, null, 0) == 1)
            || ($aAttributes['UncheckMultipleMaxHideTip'] == 0 && $this->get('hideMaxTip', null, null, 0) == 0 && trim($aAttributes['min_answers']) == '')
        ) {
            $script .= "$('#vmsg_" . $oEvent->get('qid') . "_num_answers').addClass('sr-only');\n";
        }
        App()->getClientScript()->registerScript(
            'uncheckmultiplemax' . $oEvent->get('qid'),
            $script,
            CClientScript::POS_END
        );
    }

    private function registerUncheckMultipleMaxPackage()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        /* Add package if not exist (LimeSurvey 3 have own devbridge-autocomplete) */
        if (!App()->clientScript->hasPackage('uncheckmultiplemax')) { // Not tested with 3 and older devbridge-autocomplete
            App()->clientScript->addPackage(
                'uncheckmultiplemax',
                [
                    'basePath' => get_class($this) . '.assets',
                    'js' => ['uncheckMultipleMax.js'],
                    'depends' => ['jquery'],
                ]
            );
        }
        /* Registering the package */
        App()->getClientScript()->registerPackage('uncheckmultiplemax');
    }

    /**
    * The attribute,
    */
    public function addUncheckMultipleMaxAttribute()
    {
        $oEvent = $this->getEvent();
        $UncheckMultipleMaxAttributes = array(
            'UncheckMultipleMax' => array(
                'name'      => 'UncheckMultipleMax',
                'types'     => 'M',
                'category'  => "Display",
                'sortorder' => 120,
                'inputtype' => 'singleselect',
                'options'   => array(
                    0 => $this->gT('Leave default'),
                    1 => $this->gT('Yes'),
                    -1 => $this->gT('No'),
                ),
                'caption'   => $this->gT('Disable checkbox if maximum number reached.'),
                'default'   => '0',
                'help' => $this->gT('Disable checking of a new checkbox if a maximum number of checkboxes are checked. This action displays an alert when the participant tries to check a new box.'),
            ),
            'UncheckMultipleMaxHideTip' => array(
                'name'      => 'UncheckMultipleMaxHideTip',
                'types'     => 'M',
                'category'  => "Display",
                'sortorder' => 121,
                'inputtype' => 'singleselect',
                'options'   => array(
                    0 => $this->gT('Leave default'),
                    1 => $this->gT('Yes'),
                    -1 => $this->gT('No'),
                ),
                'caption'   => $this->gT('Hide the tip.'),
                'default'   => '0',
                'help' => $this->gT('Hide the tip remlated to maximum value, if set to yes it can hide the tip for minimal value too.'),
            ),
        );
        $oEvent->append('questionAttributes', $UncheckMultipleMaxAttributes);
    }
}
