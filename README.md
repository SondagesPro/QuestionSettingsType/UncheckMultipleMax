# UncheckMultipleMax : a plugin for LimeSurvey for multiple choice question

This plugin add a javascript solution to disable checking new checkbox if maximum of checkbox are reached.

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/UncheckMultipleMax directory : `git clone https://gitlab.com/SondagesPro/QuestionSettingsType/UncheckMultipleMax.git UncheckMultipleMax`

### Via ZIP and upload (since LimeSurvey 4.X)
- [Get the zip on gitlab](https://gitlab.com/SondagesPro/QuestionSettingsType/UncheckMultipleMax/-/archive/main/UncheckMultipleMax-main.zip)
- Edit the zip and rename directory to UncheckMultipleMax
- Rename the fime to UncheckMultipleMax.zip
- Upload at LimeSurvey extension manager


## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/QuestionSettingsType/UncheckMultipleMax).

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright © 2023 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
